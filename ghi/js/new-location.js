// Listen for the DOM to be fully loaded
window.addEventListener('DOMContentLoaded', async () => {
  // Declare the API URL for fetching location data
  const url = 'http://localhost:8000/api/locations/';

  // Fetch location data from the API
  const responseNewLocation = await fetch(url);

  // Check if the response is successful
  if (responseNewLocation.ok) {
    // Parse the response body as JSON
    const data = await responseNewLocation.json();

    // Get the select element for location options
    const selectTag = document.getElementById('location');

    // Populate the select element with location options
    for (let location of data.locations) {
      // Create a new option element
      const option = document.createElement('option');

      // Set the value of the option to location's id
      option.value = location.id;

      // Set the visible text of the option to location's name
      option.innerHTML = location.name;

      // Append the option to the select element
      selectTag.appendChild(option);
    }

    // Get the form element for creating conferences
    const formTag = document.getElementById('create-conference-form');

    // Add an event listener for form submission
    formTag.addEventListener('submit', async event => {
      // Prevent the default form submission behavior
      event.preventDefault();

      // Get form data as a FormData object
      const formData = new FormData(formTag);

      // Convert FormData to a JSON string
      const json = JSON.stringify(Object.fromEntries(formData));

      // Define the conference creation API URL
      const conferenceUrl = 'http://localhost:8000/api/conferences/';

      // Configuration for the fetch request
      const fetchConfig = {
        method: 'POST',
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      // Send the POST request to create a new conference
      const response = await fetch(conferenceUrl, fetchConfig);

      // Check if conference creation was successful
      if (response.ok) {
        // Reset the form fields
        formTag.reset();

        // Parse the response body as JSON
        const newConference = await response.json();

        // Log the new conference data to the console
        console.log(newConference);
      }
    });
  }
});
