// Define a function to create an HTML card with conference details
function createCard(name, description, pictureUrl, starts, ends, location) {
  return `
    <div class="card shadow">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
          <small class="text-muted">${starts} - ${ends}</small>
      </div>
    </div>
  `;
}

// Listen for the DOMContentLoaded event and execute the following code when the page is loaded
window.addEventListener('DOMContentLoaded', async () => {

  // URL to fetch conference data from
  const url = 'http://localhost:8000/api/conferences/';

  try {
    // Fetch conference data from the provided URL
    const response = await fetch(url);

    // Check if the response is not OK
    if (!response.ok) {
      // Handle the case when the response is not successful
      throw 'Error! Something went wrong';
    } else {
      // If the response is successful, extract the JSON data from it
      const data = await response.json();

      // Initialize a counter for tracking the iteration
      let counter = 0;

      // Loop through each conference in the fetched data
      for (let conference of data.conferences) {
        // Increment the counter
        counter += 1;

        // Construct the URL to fetch detailed information about the conference
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        // Check if the detail response is OK
        if (detailResponse.ok) {
          // Extract detailed information about the conference
          const details = await detailResponse.json();

          // Extract conference details
          const pictureUrl = details.conference.location.picture_url;
          const title = details.conference.name;
          const subtitle = details.conference.location.name;

          const description = details.conference.description;

          // Convert conference start and end dates to formatted date strings
          const sDate = new Date(details.conference.starts);
          const starts = sDate.toLocaleDateString();
          const eDate = new Date(details.conference.ends);
          const ends = eDate.toLocaleDateString(sDate);

          // Create an HTML card using the createCard function and the extracted information
          const html = createCard(title, description, pictureUrl, starts, ends, subtitle);

          // Select the columns where the cards should be placed
          const column1 = document.querySelector('.col1');
          const column2 = document.querySelector('.col2');
          const column3 = document.querySelector('.col3');

          // Distribute the cards evenly across the columns based on the counter
          if (counter % 3 === 1){
            column1.innerHTML += html;
          }
          else if (counter % 3 === 2){
            column2.innerHTML += html;
          }
          else {
            column3.innerHTML += html;
          }
        }
      }
    }
  } catch (e) {
    // Handle errors by logging them to the console
    console.error(e);
  }

});
