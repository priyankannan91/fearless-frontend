// Listen for the DOM to be fully loaded
window.addEventListener('DOMContentLoaded', async () => {
  // Declare the API URL variable for location data
  const url = 'http://localhost:8000/api/locations/';

  // Fetch location data from the API
  const responseNewLocation = await fetch(url);

  // Check if the response is successful (HTTP status 200)
  if (responseNewLocation.ok) {
    // Parse the response body as JSON
    const data = await responseNewLocation.json();

    // Get the select tag element with ID 'location'
    const selectTag = document.getElementById('location');

    // Loop through each location in the fetched data
    for (let location of data.locations) {

      // Create a new 'option' element
      const option = document.createElement('option');

      // Set the 'value' attribute of the option element to the location's id
      option.value = location.id;

      // Set the visible text of the option element to the location's name
      option.innerHTML = location.name;

      // Append the option element as a child of the select tag
      selectTag.appendChild(option);
    }

    // Get the form tag element for creating conferences
    const formTag = document.getElementById('create-conference-form');

    // Add an event listener for form submission
    formTag.addEventListener('submit', async event => {
      // Prevent the default form submission behavior
      event.preventDefault();

      // Get form data as a FormData object
      const formData = new FormData(formTag);

      // Convert FormData to a JSON string
      const json = JSON.stringify(Object.fromEntries(formData));

      // Define the API URL for creating conferences
      const conferenceUrl = 'http://localhost:8000/api/conferences/';

      // Configuration for the fetch request
      const fetchConfig = {
        method: 'POST',
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      // Send the POST request to create a new conference
      const response = await fetch(conferenceUrl, fetchConfig);

      // Check if the conference creation was successful
      if (response.ok) {
        // Reset the form fields after successful submission
        formTag.reset();

        // Parse the response body as JSON
        const newConference = await response.json();

        // Log the newly created conference data to the console
        console.log(newConference);
      }
    });
  }
});
